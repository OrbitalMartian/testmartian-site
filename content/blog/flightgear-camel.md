---
title: 8 - Sopwith Camel in Flightgear
date: 2023-10-25
tags:
 - 100DaysToOffload
 - FlightGear
 - Flight sim
---

# *spit*
[Flightgear](https://flightgear.org) is my favourite flight simulator, and recently I've enjoyed flying the [Sopwith Camel](https://sites.google.com/view/fgukhangar/flightgear-uk-home-page/hangar/davinci-aircraft/warbirds/davinci_wwi_fighters?authuser=0) (by [VooDoo](https://sites.google.com/view/fgukhangar/flightgear-uk-home-page/hangar/davinci-aircraft/warbirds?authuser=0) at [FGUK](https://fguk.me)). Here's my first thoughts about the aircraft.

I've been flying the Camel for about a week now, total flight time (total time in aircraft in sim) of 07:31:24 and effective flight time (time actually in air) of 02:02:10. I enjoy flying around the UK, especially with the Orthophotos that I was given in the [FGUK forum](forum.fguk.me). FlightGear is an amazing way to enjoy the world from the comfort of your own home.

Have a couple of pictures from today.

<img src="https://cdn.discordapp.com/attachments/885162368626221096/1166811134251258057/fgfs-20231025162904.png?ex=654bd88c&is=6539638c&hm=e6eefcda3120727372181ffd52372c33dcaa34077e001fc0f258c191658d7b49&" alt="Sopwith Camel Flying" style="width:100%">

Sopwith Camel flying overhead  Wycombe Air Park.


<img src="https://cdn.discordapp.com/attachments/885162368626221096/1166811134658084874/fgfs-20231025163344.png?ex=654bd88c&is=6539638c&hm=410dac476deec7a99d790d3cd76db7ab94bb8f7fcec8baf6aaaaac7a8302db27&" alt="Sopwiyh Camel Landed At Wycombe Air Park" style="width:100%">

Sopwith Camel landed at Wycombe Air Park.

<br>

If you have any suggestions on what you want to see in my blog, hit me up on the Fediverse - [@orbitalmartian@alpha.polymaths.social](https://alpha.polymaths.social/@orbitalmartian)
