---
title: 36 - Read More, Social Less
description: I am too "addicted" to checking Fedi, I want and need to read more.
date: 2024-02-23
tags:
- Reading
- Wellbeing
- Ramblings
- 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian@alpha.polymaths.social/111982727883688360
---

I am constantly checking my Fediverse accounts which sounds as bad as it is. It's all too much. My brain only has a small capacity and all this social shit really messes with my brain. I have been trying to get into reading more which I find is a relaxing activity (much like writing). In this short blog post, I'm going to explain my plan to stop this.

My first plan was to just stop all together, but that isn't going to work as it would leaves me wanting it more. I changed my mind and have now come up with a brand new, reasonable plan.

On workdays, I will check socials once before work. Then, whilst at work I will avoid it. Once home after work, I will hop on and off until I go to bed when I will put my phone down and forget about things, until I do it all again the next day.

On weekends and non-working days, I will be on and off socials (mostly Fedi) and once I start gaming or doing something else that I can set my mind to, I will remain off socials. Then the evenings will be the same as workdays.

I often have headaches from either using a screen for too long or not drinking enough water. I hope that this plan will help me with this and that I'll be able to stick with it.

Anyway, that's it from me for this one, I hopefully, by the time you are reading this, have proofread and fixed any and all the various typos I've made in writing. That's it for tonight, I'll see you in the next one.


<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>

