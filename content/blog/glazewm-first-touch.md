---
title: 46 - GlazeWM - First Touch
date: 2024-04-17
tags:
- Software
- Windows 11
- Windows
- Review
- 100DaysToOffload
---

Hey folks, I have never been one to share any sort of affection for anything Windows related. But this is a rare occasion that I never saw coming. I was perusing my YouTube feed and saw a video by [ChrisTitusTech](https://youtu.be/0I8HyVMKEeo?si=bCupk64hT9wqxL1l) about a tiling window manager for Windows, as a TWM fan, I felt the urge to give it a go.

It's called [GlazeWM](https://github.com/glzr-io/glazewm) and is inspired by [i3](https://i3wm.org/) and [Polybar](https://polybar.github.io/) (which, back when I first tried tiling window managers, was my setup). Running as an application (in `.exe` format), there is no need to install it, just run from the executable. I had a very minimal experience with it as I haven't had too much of a chance to actually play around with it. It seems a very interesting project and I have a feeling that it could go places.

I still need to have a proper play around with it, but for a quick first look it was amazing and the project's documentation is very thorough and with such an easy install, I see myself continuing with testing it out. I also haven't even covered the custom configuration yet, which is in [YAML](https://yaml.org/) (I have limited experience in YAML but I can research and the GlazeWM documentation is very well put together, so I'm sure I can figure it out).

That is it for this one, have a good one, and enjoy!

<aside>
<h3>Link Index</h3>
<ul>
<li><a href="https://youtu.be/0I8HyVMKEeo?si=bCupk64hT9wqxL1l">ChrisTitusTech's video</a></li>
<li><a href="https://github.com/glzr-io/glazewm">GlazeWM Git repo</a></li>
<li><a href="https://i3wm.org/">i3 Website</a></li>
<li><a href="https://polybar.github.io/">Polybar Website (which I didn't know existed)</a></li>
<li><a href="https://yaml.org/">YAML Website</a></li>
</ul>
</aside>

