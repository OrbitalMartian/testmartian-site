---
title: 10 - Christmas But Early
date: 2023-10-27
tags:
 - 100DaysToOffload
 - Random Thoughts
 - Christmas
---

# Ho Ho Ho
One of my favourite times of the year is Christmas, mostly because of the chocolate and presents XD.

For real though, it is a magical time of year and it's a time to be with your family, and to spread love and joy. So spend it with your loved ones :).


I'm running low on ideas for my blog - if you have any suggestions on what you want to see in my blog, hit me up on the Fediverse - [@orbitalmartian@alpha.polymaths.social](https://alpha.polymaths.social/@orbitalmartian)
