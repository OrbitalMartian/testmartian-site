---
title: 32 - Alex Rider Eagle Strike - Book Review - 4/2/24
date: 2024-02-04
tags:
 - Book Reviews
 - 100DaysToOffload
---

I'm currently reading Ravens Gate by Anthony Horowitz.

### Pages: 34/352

Really enjoying it, reading is very relaxing and this book has a really interesting storyline and it's already picking up.

[View this progress review on Bookwyrm](https://bookwyrm.social/user/OrbitalMartian/comment/3763200)

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>