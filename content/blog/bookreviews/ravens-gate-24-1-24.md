---
title: 27 - Ravens Gate - Book Review - 24/1/24
date: 2024-01-24
tags:
 - Book Reviews
 - 100DaysToOffload
---

I'm currently reading Ravens Gate by Anthony Horowitz.

### Pages: 141/304

Had a decent amount of reading over the weekend, really enjoyed it and it definitely was a mindfulness exercise and helped me to relax and unwind.

Really love the story.

[View this progress review on Bookwyrm](https://bookwyrm.social/user/OrbitalMartian/comment/3658506)

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>