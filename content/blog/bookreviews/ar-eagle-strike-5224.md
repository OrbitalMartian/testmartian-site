---
title: 33 - Alex Rider Eagle Strike - Book Review - 5/2/24
date: 2024-02-05
tags:
 - Book Reviews
 - 100DaysToOffload
---

I'm currently reading Alex Rider - Eagle Strike by Anthony Horowitz.

### Pages: 43/352

<details>
<summary>Spoilers ahead! - click to expand.</summary>
<div>
I have found the day's reading very relaxing, a huge event has taken place that adds an emotional side to the story.
<br>
With Sabina's dad being hurt by an explosion, the story has already become further than I'd expect a story to be at this stage.
</div>
</details>


[View this progress review on Bookwyrm](https://bookwyrm.social/user/OrbitalMartian/comment/3771857)

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>