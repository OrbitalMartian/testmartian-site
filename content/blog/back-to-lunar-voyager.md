---
title: 22 - I'm Going Back To Lunar Voyager
date: 2023-12-09
description: I started writing a novel called Lunar Voyager for NaNoWriMo, and now I'm coming back to writing it.
tags:
- NaNoWriMo
- Writing
- 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian/111553129398987429
---

I started writing a novel for  [NaNoWriMo](https://nanowrimo.org/) called Lunar Voyager. From tomorrow, I'm going to come back to it and continue writing the novel, keeping track of my words and having a goal for the day/week/month.

I will, of course, be open to feedback and I do encourage this.

My original Fediverse post about my novel is below :) 

<iframe src="https://linuxrocks.online/@orbitalmartian/111365919251007778/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe><script src="https://linuxrocks.online/embed.js" async="async"></script>


<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>