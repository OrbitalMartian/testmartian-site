---
title: 20 - This Site Is Powered By 11ty
description: 11ty is a simple and user friendly static site generator - which powers this site.
date: 2023-11-27
tags:
- 11ty
- Web Dev
- 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian/111484342958208015
---

I have been playing around with my site here for a reason. I have been playing with static site generators for about a year now. I've used a few, from Jekyll on Github to Eleventy, and by far, my favourite has been Eleventy. Granted I am still using the base blog template, but I have got a thoughts and am asking for help!

My thoughts... I love the simplicity and automation of everything, it's really intuitive and fit into my workflow perfectly. Now onto the bad side, I do enjoy the aesthetic of this simple site theme, however I want something a bit more, I have outlined my [ideas](https://orbitalmartian.codeberg.page/blog/site-plans/) in a previous blog post, but have been struggling to action some of the changes, my mind just hasn't been focused. I am hereby asking for help, if you have any knowledge if Eleventy and are willing to help, please check out my [Codeberg repo](https://codeberg.org/OrbitalMartian/eleventy) and have a look at the issues and if you see one you can help on, let me know and I'll assign you to the issue.

I have managed to action one thing, that is the description on the post list. Everything else is still to come.


Hope you enjoyed and thanks in advance to any help I get.

<img src="/img/Written-By-Human-Not-By-AI-Badge-black.png" alt="Not Written By AI badge">

