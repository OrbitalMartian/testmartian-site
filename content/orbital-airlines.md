---
layout: layouts/home.njk
permalink: /oa/
---

# Orbital Airlines
Orbital Airlines (OA) is a Virtual Airline for the Free and Open Source flight simulator, FlightGear.

A dedicated site is being developed but for now this is the base of all knowledge.


## Fleet
I am currently working on liveries for our fleet but it will be likely that the first livery will be for the King Air B100 and C90A.


## Scheduled Flights
No scheduled flights yet. There will be some flights which get flown often but as of yet, there are none.
